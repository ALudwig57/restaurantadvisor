import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Search from "./src/Component/Search";
import Exercice1 from "./src/Component/Exercice1";
import Flexercice from './src/Component/Flexercice';
import RestaurantItem from './src/Component/RestaurantItem';
import Navigation from './src/navigation/Navigation';

export default function App() {
  return (
    <Navigation />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
