export const assets = {
    sourceCostIcon:     require('../../assets/icons/cost.png'),
    sourceCommentsIcon: require('../../assets/icons/comments.png'),
    sourceUtensilsIcon: require('../../assets/icons/utensils.png'),
    errorIcon : require('../../assets/icons/error.png')
}