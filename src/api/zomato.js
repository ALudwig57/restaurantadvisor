const API_KEY = '7605ddc400e3c3dec0ec0d91bcfce784';
const LONDON_ID = 61;
export async function getRestaurantsInACityWithSearch( searchTerm, cityID, startOffset ) {
    try {
      const myHeaders = new Headers({ 'user-key': API_KEY });
      const url = `https://developers.zomato.com/api/v2.1/search?entity_id=${cityID || LONDON_ID}&entity_type=city&q=${searchTerm || '%20'}&start=${startOffset || '0'}`;  
  
      const response = await fetch(url, { headers: myHeaders });
      if (response.ok) {
        return response.json();
      }
      throw new Error(response.status);
  
    } catch (error) {
      console.log('Error with function getRestaurantsInACityWithSearch ' + error.message);
      throw error;
    }
  }
  export async function getRestaurantDetails( restaurantID ) {
    try {
  
      const myHeaders = new Headers({ 'user-key': API_KEY });
      const url = `https://developers.zomato.com/api/v2.1/restaurant?res_id=${restaurantID}`;
      const response = await fetch(url, { headers: myHeaders });
  
      if (response.ok) {
        return response.json();
      }
      throw new Error(response.status);
  
    } catch (error) {
      console.log('Error with function getRestaurantDetails ' + error.message);
      throw error;
    }
  }