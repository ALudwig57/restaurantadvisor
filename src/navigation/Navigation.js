import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Search from '../Component/Search';
import Restaurant from '../Component/Restaurant';

const SearchNavigation = createStackNavigator({
    Search: Search,
    Restaurant: Restaurant,
},
{
    initialRouteName: 'Search',
});

export default createAppContainer(SearchNavigation);