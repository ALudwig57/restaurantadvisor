import React, { useState, useRef } from "react";
import { Dimensions,Text, View, Keyboard, FlatList, TextInput, Button, StyleSheet } from "react-native";
import { fakeRestaurants } from '../helpers/restaurantsFakeData';
import RestaurantItem from "./RestaurantItem";
import { getRestaurantsInACityWithSearch } from '../api/zomato';
import ErrorDisplayer from "./ErrorDisplayer";
import { colors } from "../Definition/color";

const Search = ({ navigation }) => {
    

    const [restaurants, setRestaurants] = useState([]);
    const searchTerm = useRef("");
    const paginationData = useRef({ currentOffset: 0, maxResults: 0 });
    const [isRefreshing, setRefreshingState] = useState(false);
    const [isErrorDuringDataLoading, setErrorDataLoading] = useState(false);

    _navigateToRestaurantDetails = ( restaurantID ) => {
        navigation.navigate("Restaurant", { restaurantID });
      }

    var _loadRestaurants = async (prevRestaurantResults) => {
        setRefreshingState(true);
        setErrorDataLoading(false);

        try {
            var zomatoSearchResult = [];
            zomatoSearchResult = (await getRestaurantsInACityWithSearch(searchTerm.current, paginationData.current.currentOffset));
            paginationData.current = { currentOffset: paginationData.current.currentOffset + zomatoSearchResult.results_shown, maxResults: zomatoSearchResult.results_found }
            setRestaurants([...prevRestaurantResults, ...zomatoSearchResult.restaurants])
        }
        catch (error) {
            paginationData.current = {currentOffset: 0, maxResults:0}
            setRestaurants( [] );
            setErrorDataLoading( true );
        }
        finally {
            setRefreshingState( false );
        }
    }

    var _inputSearchTermChanged = (text) => {
        searchTerm.current = text;
    } 

    var _searchRestaurants = async () => {
        paginationData.current = { currentOffset: 0, maxResults: 0 }
        _loadRestaurants([]);
    }

    var _loadMoreRestaurants = () => {
        if (paginationData.current.currentOffset < paginationData.current.maxResults) {
            _loadRestaurants(restaurants);
        }
    }

    return (
        <View style={styles.mainView}>
            <TextInput
                style={styles.placeholder}
                onChangeText={text => _inputSearchTermChanged(text)}
                onSubmitEditing={_searchRestaurants}
                placeholder='Nom du restaurant' />
            <Button
                title='Rechercher'
                onPress={() => {_searchRestaurants(); Keyboard.dismiss()}}
                
                color={colors.mainGreenColor}/>
            { isErrorDuringDataLoading ? (
                <ErrorDisplayer errorMessage='Impossible de charger les restaurants'/>
            ): (
            <FlatList 
                data = { restaurants }
                keyExtractor = { (item) => item.restaurant.id.toString() }
                renderItem = { ({ item }) => <RestaurantItem 
                                            restaurant = {item.restaurant } 
                                            onClickOnMe={ () => _navigateToRestaurantDetails(item.restaurant.id) }
                                            />}
                onEndReached={_loadMoreRestaurants}
                onEndReachedThreshold={0.5}
                onRefresh={_searchRestaurants}
                refreshing={isRefreshing}
            />
                )}
        </View>
    );
};

Search.navigationOptions = {     
    title: 'Recherche',     
    headerTitleStyle: {         
        width: Dimensions.get('window').width - 75     
    } 
}

export default Search;
const styles = StyleSheet.create({
    mainView: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    searchField: {
        padding: 5,
    }
})