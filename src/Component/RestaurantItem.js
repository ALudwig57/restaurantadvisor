import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';

import { colors } from '../Definition/color';
import { assets } from '../Definition/assets';
import { TouchableOpacity } from 'react-native-gesture-handler';

const RestaurantItem = ({ restaurant, restaurant : {user_rating}, onClickOnMe }) => {
  return (
    <TouchableOpacity 
    style={ styles.mainContainer }
    onPress={ onClickOnMe }>

      <Image style={ styles.restaurantImage }
              source={{ uri: restaurant.thumb }}/>
      <View style={ styles.itemsContainer }>
        <View style={ styles.restaurantTextContainer }>
          <Text style={ styles.restaurantNameText }>
            {restaurant.name }
          </Text>
          <Text 
            style={ styles.restaurantCuisinesText }
            numberOfLines={ 1 }> 
            {restaurant.cuisines }
          </Text>
        </View>
        <View style={ styles.ratesContainer }>
          <View style={ styles.markContainer }>
            <Image style={ styles.restaurantIcons }
                    source={ assets.sourceUtensilsIcon}/>
            <Text style={ styles.restaurantText }>{restaurant.user_rating.aggregate_rating}</Text>
          </View>
          <View style={ styles.commentContainer }>
            <Image style={ styles.restaurantIcons }
                    source={ assets.sourceCommentsIcon}/>
            <Text style={ styles.restaurantText }>{restaurant.user_rating.votes}</Text>
          </View>
          <View style={ styles.priceContainer }>
            <Image style={ styles.restaurantIcons }
                    source={ assets.sourceCostIcon}/>
            <Text style={ styles.restaurantText }>{restaurant.average_cost_for_two / 2}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default RestaurantItem;

const styles = StyleSheet.create({
  mainContainer: {
    paddingVertical: 25,
    paddingHorizontal: 15,
    flexDirection: "row",
  },
  restaurantImage: {
    height: 80,
    width: 80,
    backgroundColor: colors.mainGreenColor,
  },
  itemsContainer: {
    flex: 1,
    marginLeft: 10,
  },
  restaurantTextContainer: {
  },
  restaurantNameText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  restaurantCuisinesText: {
    fontStyle: 'italic',
    fontSize: 16,
  },
  restaurantIcons: {
    height: 18,
    width: 18,
    tintColor: colors.mainGreenColor,
  },
  restaurantText: {
    fontSize: 15,
    paddingLeft: 2,
  },
  ratesContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  markContainer: {
    flexDirection: 'row',
    marginRight: 10,
  },
  commentContainer: {
    flexDirection: 'row',
    marginRight: 10,
  },
  priceContainer: {
    flexDirection: 'row',
  }
});