import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, ScrollView, Image } from 'react-native';

import { getRestaurantDetails } from '../api/zomato';

import { colors } from '../Definition/color';

const Restaurant = ({ navigation }) => {
    const [isLoading, setLoadingState] = useState(true);
    const [restaurantData, setRestaurantData] = useState(null);

    useEffect(() => {
        _loadRestaurant();
    }, []);

    var _loadRestaurant = async () => {
        try {
            setRestaurantData(await getRestaurantDetails(navigation.getParam('restaurantID')));
            setLoadingState(false);
        } catch (error) {
            //Do 
        }
    }

    const _displayLoading = () => {
        if (isLoading) {
            return (
                <View style={styles.loadingView}>
                    <ActivityIndicator size="large" />
                </View>
            );
        }
        return null;
    }

    const _displayTimings = () => {
        let timingsList = restaurantData.timings.split(",");
        let timingsJSX = [];
        timingsList.forEach((timing, index) => {
            timingsJSX.push(<Text key={index} style={styles.contentText}>{timing}</Text>)
        });
        return (
            <View>
                {timingsJSX}
            </View>
        );
    }

    const _displayRestaurant = () => {
        if (restaurantData) {
            return (
                <ScrollView style={styles.scrollViewRestaurant}>
                    <Image style={styles.restaurantImage}
                        source={{ uri: restaurantData.featured_image }} />
                    <View style={styles.topCard}>
                        <View style={styles.estabView}>
                            <Text style={styles.nameText}>
                                {restaurantData.name}
                            </Text>
                            <Text style={styles.establishmentText}
                                numberOfLines={1}>
                                {restaurantData.establishment.join()}
                            </Text>
                        </View>
                        <View style={styles.noteContainer}>
                            <View style={[styles.noteView, { backgroundColor: ('#' + restaurantData.user_rating.rating_color) }]}>
                                <Text style={styles.noteText}>
                                    {restaurantData.user_rating.aggregate_rating}
                                </Text>
                                <Text style={styles.maxNoteText}>
                                    /5
                                </Text>
                            </View>
                            <Text style={styles.votesText}>
                                {restaurantData.user_rating.votes} votes
                            </Text>
                        </View>
                    </View>
                    <View style={styles.bottomCard}>
                        <Text style={[styles.titlesText, { marginTop: 0 }]}>
                            Cuisines
                        </Text>
                        <Text style={styles.contentText}>
                            {restaurantData.cuisines}
                        </Text>
                        <Text style={styles.titlesText}>
                            Numéro(s) de téléphone
                        </Text>
                        <Text style={styles.contentText}>
                            {restaurantData.phone_numbers}
                        </Text>
                        <Text style={styles.titlesText}>
                            Adresse
            </Text>
                        <Text style={styles.contentText}>
                            {restaurantData.location.address}
                        </Text>
                        <Text style={styles.titlesText}>
                            Horaires d'ouverture
            </Text>
                        {_displayTimings()}
                    </View>
                </ScrollView>
            );
        }
        return null;
    }

    return (
        <View style={styles.mainView}>
            {_displayLoading()}
            {_displayRestaurant()}
        </View>
    );
}

export default Restaurant;

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    loadingView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollViewRestaurant: {
        flex: 1,
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
    },
    restaurantImage: {
        height: 180,
        backgroundColor: colors.mainGreenColor,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
    },
    topCard: {
        elevation: 1,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    bottomCard: {
        marginTop: 20,
        marginBottom: 20,
        elevation: 1,
        borderRadius: 5,
        backgroundColor: 'white',
        paddingBottom: 25,
        paddingTop: 15,
        paddingLeft: 10,
    },
    estabView: {
        flex: 4,
        paddingLeft: 10,
        paddingTop: 5,
        paddingBottom: 25,
    },
    noteContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    noteView: {
        padding: 5,
        borderRadius: 3,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    noteText: {
        color: 'white',
        fontWeight: "bold",
        fontSize: 16,
    },
    maxNoteText: {
        fontSize: 12,
        marginLeft: 3,
        color: 'white',
    },
    votesText: {
        fontStyle: "italic",
        fontSize: 10,
    },
    nameText: {
        fontSize: 23,
        fontWeight: 'bold',
    },
    establishmentText: {
        fontSize: 18,
    },
    titlesText: {
        fontWeight: 'bold',
        color: colors.mainGreenColor,
        fontSize: 21,
        marginTop: 20,
    },
    contentText: {
        fontSize: 18,
    },

});
