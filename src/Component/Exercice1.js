import React from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";
import { colors } from "../Definition/color";
const Exercice1 = () => {
    return (
        <View style={styles.mainView}>
            <TextInput placeholder='Inserer votre nom' />
            <TextInput placeholder='Inserer votre prénom' />
            <Text style={styles.m2}>M2GI</Text>
            <Text style={styles.m2}>2019 - 2020</Text>
            <Text style={styles.univ}>Université de Lorraine</Text>
            <Button
                title='Ajouter'
                onPress={maFonction}
                color={colors.mainGreenColor}
            />
        </View>
    );
}

maFonction = function () {
    console.log('Coucou')

}

export default Exercice1;
const styles = StyleSheet.create({
    mainView: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    searchField: {
        padding: 5,
    },
    m2: {
        fontWeight: 'bold',
        color: colors.mainGreenColor,
        fontSize: 23
    },
    univ: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontSize: 23
    },

})