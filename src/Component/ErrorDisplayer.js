import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { colors } from '../Definition/color';
import { assets } from '../Definition/assets';
const ErrorDisplayer = ({errorMessage}) => {
    return (
        <View style={StyleSheet.mainView}>
            <Image style= {styles.errorImage}
                    source= { assets.errorIcon }/>
            <Text style= {styles.errorText}>{ errorMessage }</Text>
        </View>
    )
}

export default ErrorDisplayer;

const styles = StyleSheet.create({
    mainView: {
      flex: 1,
      padding: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    errorImage: {
      width: 100,
      height: 100,
      tintColor: colors.mainGreenColor,
      marginBottom: 30,
      marginTop: 30,
      alignSelf: 'center'

    },
    errorText: {
      fontSize: 19,
      fontStyle:'italic',
      textAlign: 'center',
      marginBottom: 100,
    },
  });