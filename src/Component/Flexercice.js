import React from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";
import { colors } from "../Definition/color";
const Flexercice = () => {
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 2, flexDirection: 'row' }}>
                <View style={styles.rectBleu}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={styles.petitCarreJaune1} />
                        <View style={styles.petitCarreJaune2} />
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'space-around' }}>
                    <View style={styles.petitCarreJaune3} />
                </View>
                <View style={{ flex: 1 }}>
                    <View style={styles.rectRouge} />
                    <View style={styles.carreJaune} />
                </View>
            </View>
            <View style={styles.rectCyan}>
                <View style={{ flex: 1, flexDirection: 'row' }} />
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1 }} />
                    <View style={styles.carreJaune} />
                    <View style={{ flex: 1 }} />
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }} />
            </View>
        </View>
    );
}

maFonction = function () {
    console.log('Coucou')

}

export default Flexercice;
const styles = StyleSheet.create({
    rectRouge: {
        flex: 4,
        backgroundColor: 'red'
    },
    rectBleu: {
        flex: 2,
        backgroundColor: 'blue'
    },
    rectCyan: {
        flex: 1,
        backgroundColor: 'cyan'
    },
    petitCarreJaune1: {
        width: 50,
        height: 50,
        backgroundColor: 'yellow',
        alignSelf: 'flex-start'
    },
    petitCarreJaune2: {
        width: 50,
        height: 50,
        backgroundColor: 'yellow',
        alignSelf: 'flex-end'
    },
    petitCarreJaune3: {
        width: 50,
        height: 50,
        backgroundColor: 'yellow',
        alignSelf: 'center'
    },
    carreJaune: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    mainView: {
        marginTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    searchField: {
        padding: 5,
    },
})